<?php
/**
 * class.onlineHelp.php
 *
 */

class onlineHelpClass extends PMPlugin
{
    public function __construct()
    {
        set_include_path(
          PATH_PLUGINS . 'onlineHelp' . PATH_SEPARATOR .
          get_include_path()
        );
    }

    public function setup()
    {
    }

    public function getFieldsForPageSetup()
    {
    }

    public function updateFieldsForPageSetup()
    {
    }
}

