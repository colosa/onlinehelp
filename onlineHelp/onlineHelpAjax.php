<?php
// Include required files
set_include_path(PATH_PLUGINS . 'onlineHelp' . PATH_SEPARATOR . get_include_path());
require_once 'classes/model/OhConfiguration.php';

// General Validations
if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = '';
}
if (!isset($_REQUEST['limit'])) {
    $_REQUEST['limit'] = '';
}
if (!isset($_REQUEST['start'])) {
    $_REQUEST['start'] = '';
}
// Initialize response object
$response = new stdclass();
$response->status = 'OK';

// Main switch
try {
    switch ($_REQUEST['action']) {
        case 'listOnlineHelp':
            $sort = (isset($_REQUEST['sort'])) ? $_REQUEST['sort'] : '' ;
            $dir = (isset($_REQUEST['dir'])) ? $_REQUEST['dir'] : 'ASC' ;
            // List
            $criteria = new Criteria();
            $criteria->addSelectColumn('COUNT(*)');
            $criteria->addSelectColumn(OhConfigurationPeer::OH_UID);
            $result = OhConfigurationPeer::doSelectRS($criteria);
            $result->setFetchmode(ResultSet::FETCHMODE_ASSOC);
            $result->next();
            $totalCount = $result->getRow();
            $totalCount = $totalCount['COUNT(*)'];

            $criteria = new Criteria();

            if ($sort != '') {
                if ($dir == 'ASC') {
                    $criteria->addAscendingOrderByColumn(OhConfigurationPeer::OH_FOLDER);
                } else {
                    $criteria->addDescendingOrderByColumn(OhConfigurationPeer::OH_FOLDER);
                }
            } else {
                $criteria->addDescendingOrderByColumn(OhConfigurationPeer::OH_FOLDER);
                $criteria->addDescendingOrderByColumn(OhConfigurationPeer::OH_FILE);
                $criteria->addDescendingOrderByColumn(OhConfigurationPeer::OH_OPTION);
            }
            $criteria->setLimit( $_REQUEST['limit'] );
            $criteria->setOffset( $_REQUEST['start'] );
            $result = OhConfigurationPeer::doSelectRS($criteria);
            $result->setFetchmode(ResultSet::FETCHMODE_ASSOC);
            $data = Array();
            while ($result->next()) {
                $data[] = $result->getRow();
            }
            $response = array();
            $response['totalCount'] = $totalCount;
            $response['data']       = $data;
            break;
        case 'deleteOnlineHelp':
            // Para borrar
            $ohConfigurationInstance = new OhConfiguration();
            try {
                $ohConfigurationInstance->deleteByUid($_REQUEST['OH_UID']);
                $response->success = true;
            } catch (Exception $error) {
                throw $error;
            }
            break;
        case 'saveOnlineHelp':
            // Save configuration
            $ohConfigurationInstance = new OhConfiguration();
            try {
                $response->OH_UID = $ohConfigurationInstance->createOrUpdate($_REQUEST);
                $response->success = true;
            } catch (Exception $error) {
                throw $error;
            }
            break;
        case 'popUp':
            $src = isset($_REQUEST['src']) ? $_REQUEST['src'] : '';
            $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
            $debugMode = isset($_REQUEST['debug_mode']) ? $_REQUEST['debug_mode'] : 'view';
            $selection = isset($_REQUEST['Data']) ? htmlentities($_REQUEST['Data'], ENT_QUOTES, "UTF-8") : '';
            $arraySearch = explode( '/', $selection);
            $search = explode( '?', isset($arraySearch[7]) ? $arraySearch[7] : '');

            $criteria = new Criteria();
            $criteria->add( OhConfigurationPeer::OH_FOLDER, isset($arraySearch[6]) ? $arraySearch[6] : ''  );

            $criteria->add( $criteria->getNewCriterion( OhConfigurationPeer::OH_FILE, '%' . $search[0] . '%', Criteria::LIKE ));
            if (isset($search[1])) {
                $options = explode( '&', $search[1]);
                foreach ($options as $option) {
                    $criteria->add( $criteria->getNewCriterion( OhConfigurationPeer::OH_OPTION, '%' . $option . '%', Criteria::LIKE ));
                }
            } else {
                $search[1] = '';
            }
            $result = OhConfigurationPeer::doSelectRS($criteria);
            $result->setFetchmode(ResultSet::FETCHMODE_ASSOC);
            $data = Array();
            $debugUid = '';
            $debugTarget = '';
            while ($result->next()) {
                $row = $result->getRow();
                $debugUid =  $row['OH_UID'];
                $debugTarget =  $row['OH_LINK'];
                if ($debugMode != 'importURL') {
                    header("Location:". $row['OH_LINK']);
                    die();
                }
            }
            $response->data = array ('debugUid' => $debugUid ,
                                     'debugFolder' => isset($arraySearch[6]) ? $arraySearch[6] : '',
                                     'debugFile' => $search[0],
                                     'debugOption' => $search[1],
                                     'debugTarget' => $debugTarget,
                                     'debugURL' => $selection
                                     );
            if ($debugMode != 'importURL') {
                header("Location:". 'http://wiki.processmaker.com/index.php?title=Special%3ASearch&search='. $arraySearch[6] .'&go=Go');
                die();
            }
            break;
    }
} catch (Exception $error) {
    $response = new stdclass();
    $response->status = 'ERROR';
    $response->message = $error->getMessage();
}

header('Content-Type: application/json;');
die(G::json_encode($response));

