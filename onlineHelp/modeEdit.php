<?php
$oHeadPublisher = &headPublisher::getSingleton();
$oHeadPublisher->addExtJsScript('onlineHelp/admin', false );

set_include_path(PATH_PLUGINS . 'onlineHelp' . PATH_SEPARATOR . get_include_path());
require_once 'classes/model/OhConfiguration.php';

$src = $_REQUEST['src'];
$id = $_REQUEST['id'];
$selection = $_REQUEST["Data"];
$arraySearch = explode( '/', $selection);
$search = explode( '?', $arraySearch[7]);

$criteria = new Criteria();
$criteria->add( OhConfigurationPeer::OH_FOLDER, $arraySearch[6] );

$criteria->add( $criteria->getNewCriterion( OhConfigurationPeer::OH_FILE, '%' . $search[0] . '%', Criteria::LIKE ));
if (isset($search[1])) {
    $options = explode( '&', $search[1]);
    foreach ($options as $option) {
        $criteria->add( $criteria->getNewCriterion( OhConfigurationPeer::OH_OPTION, '%' . $option . '%', Criteria::LIKE ));
    }
} else {
    $search[1] = '';
}
$result = OhConfigurationPeer::doSelectRS($criteria);
$result->setFetchmode(ResultSet::FETCHMODE_ASSOC);
$data = Array();
$debugUid = '';
$debugTarget = '';
while ($result->next()) {
    $row = $result->getRow();
    $debugUid =  $row['OH_UID'];
    $debugTarget =  $row['OH_LINK'];
    //    die();
}

$oHeadPublisher->assign( 'debugMode', 'edit' );
$oHeadPublisher->assign( 'debugUid', $debugUid );
$oHeadPublisher->assign( 'debugFolder', $arraySearch[6] );
$oHeadPublisher->assign( 'debugFile', $search[0] );
$oHeadPublisher->assign( 'debugOption', $search[1] );
$oHeadPublisher->assign( 'debugTarget', $debugTarget);
$oHeadPublisher->assign( 'debugURL', $selection);

G::RenderPage('publish', 'extJs');

