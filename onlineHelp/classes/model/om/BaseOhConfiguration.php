<?php

require_once 'propel/om/BaseObject.php';

require_once 'propel/om/Persistent.php';


include_once 'propel/util/Criteria.php';

include_once 'classes/model/OhConfigurationPeer.php';

/**
 * Base class that represents a row from the 'OH_CONFIGURATION' table.
 *
 * 
 *
 * @package    workflow.classes.model.om
 */
abstract class BaseOhConfiguration extends BaseObject implements Persistent
{

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        OhConfigurationPeer
    */
    protected static $peer;

    /**
     * The value for the oh_uid field.
     * @var        string
     */
    protected $oh_uid = '';

    /**
     * The value for the oh_title field.
     * @var        string
     */
    protected $oh_title = '';

    /**
     * The value for the oh_folder field.
     * @var        string
     */
    protected $oh_folder = '';

    /**
     * The value for the oh_file field.
     * @var        string
     */
    protected $oh_file = '';

    /**
     * The value for the oh_option field.
     * @var        string
     */
    protected $oh_option = '';

    /**
     * The value for the oh_link field.
     * @var        string
     */
    protected $oh_link = '';

    /**
     * The value for the oh_create_date field.
     * @var        int
     */
    protected $oh_create_date;

    /**
     * The value for the oh_update_date field.
     * @var        int
     */
    protected $oh_update_date;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Get the [oh_uid] column value.
     * 
     * @return     string
     */
    public function getOhUid()
    {

        return $this->oh_uid;
    }

    /**
     * Get the [oh_title] column value.
     * 
     * @return     string
     */
    public function getOhTitle()
    {

        return $this->oh_title;
    }

    /**
     * Get the [oh_folder] column value.
     * 
     * @return     string
     */
    public function getOhFolder()
    {

        return $this->oh_folder;
    }

    /**
     * Get the [oh_file] column value.
     * 
     * @return     string
     */
    public function getOhFile()
    {

        return $this->oh_file;
    }

    /**
     * Get the [oh_option] column value.
     * 
     * @return     string
     */
    public function getOhOption()
    {

        return $this->oh_option;
    }

    /**
     * Get the [oh_link] column value.
     * 
     * @return     string
     */
    public function getOhLink()
    {

        return $this->oh_link;
    }

    /**
     * Get the [optionally formatted] [oh_create_date] column value.
     * 
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                          If format is NULL, then the integer unix timestamp will be returned.
     * @return     mixed Formatted date/time value as string or integer unix timestamp (if format is NULL).
     * @throws     PropelException - if unable to convert the date/time to timestamp.
     */
    public function getOhCreateDate($format = 'Y-m-d H:i:s')
    {

        if ($this->oh_create_date === null || $this->oh_create_date === '') {
            return null;
        } elseif (!is_int($this->oh_create_date)) {
            // a non-timestamp value was set externally, so we convert it
            $ts = strtotime($this->oh_create_date);
            if ($ts === -1 || $ts === false) {
                throw new PropelException("Unable to parse value of [oh_create_date] as date/time value: " .
                    var_export($this->oh_create_date, true));
            }
        } else {
            $ts = $this->oh_create_date;
        }
        if ($format === null) {
            return $ts;
        } elseif (strpos($format, '%') !== false) {
            return strftime($format, $ts);
        } else {
            return date($format, $ts);
        }
    }

    /**
     * Get the [optionally formatted] [oh_update_date] column value.
     * 
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                          If format is NULL, then the integer unix timestamp will be returned.
     * @return     mixed Formatted date/time value as string or integer unix timestamp (if format is NULL).
     * @throws     PropelException - if unable to convert the date/time to timestamp.
     */
    public function getOhUpdateDate($format = 'Y-m-d H:i:s')
    {

        if ($this->oh_update_date === null || $this->oh_update_date === '') {
            return null;
        } elseif (!is_int($this->oh_update_date)) {
            // a non-timestamp value was set externally, so we convert it
            $ts = strtotime($this->oh_update_date);
            if ($ts === -1 || $ts === false) {
                throw new PropelException("Unable to parse value of [oh_update_date] as date/time value: " .
                    var_export($this->oh_update_date, true));
            }
        } else {
            $ts = $this->oh_update_date;
        }
        if ($format === null) {
            return $ts;
        } elseif (strpos($format, '%') !== false) {
            return strftime($format, $ts);
        } else {
            return date($format, $ts);
        }
    }

    /**
     * Set the value of [oh_uid] column.
     * 
     * @param      string $v new value
     * @return     void
     */
    public function setOhUid($v)
    {

        // Since the native PHP type for this column is string,
        // we will cast the input to a string (if it is not).
        if ($v !== null && !is_string($v)) {
            $v = (string) $v;
        }

        if ($this->oh_uid !== $v || $v === '') {
            $this->oh_uid = $v;
            $this->modifiedColumns[] = OhConfigurationPeer::OH_UID;
        }

    } // setOhUid()

    /**
     * Set the value of [oh_title] column.
     * 
     * @param      string $v new value
     * @return     void
     */
    public function setOhTitle($v)
    {

        // Since the native PHP type for this column is string,
        // we will cast the input to a string (if it is not).
        if ($v !== null && !is_string($v)) {
            $v = (string) $v;
        }

        if ($this->oh_title !== $v || $v === '') {
            $this->oh_title = $v;
            $this->modifiedColumns[] = OhConfigurationPeer::OH_TITLE;
        }

    } // setOhTitle()

    /**
     * Set the value of [oh_folder] column.
     * 
     * @param      string $v new value
     * @return     void
     */
    public function setOhFolder($v)
    {

        // Since the native PHP type for this column is string,
        // we will cast the input to a string (if it is not).
        if ($v !== null && !is_string($v)) {
            $v = (string) $v;
        }

        if ($this->oh_folder !== $v || $v === '') {
            $this->oh_folder = $v;
            $this->modifiedColumns[] = OhConfigurationPeer::OH_FOLDER;
        }

    } // setOhFolder()

    /**
     * Set the value of [oh_file] column.
     * 
     * @param      string $v new value
     * @return     void
     */
    public function setOhFile($v)
    {

        // Since the native PHP type for this column is string,
        // we will cast the input to a string (if it is not).
        if ($v !== null && !is_string($v)) {
            $v = (string) $v;
        }

        if ($this->oh_file !== $v || $v === '') {
            $this->oh_file = $v;
            $this->modifiedColumns[] = OhConfigurationPeer::OH_FILE;
        }

    } // setOhFile()

    /**
     * Set the value of [oh_option] column.
     * 
     * @param      string $v new value
     * @return     void
     */
    public function setOhOption($v)
    {

        // Since the native PHP type for this column is string,
        // we will cast the input to a string (if it is not).
        if ($v !== null && !is_string($v)) {
            $v = (string) $v;
        }

        if ($this->oh_option !== $v || $v === '') {
            $this->oh_option = $v;
            $this->modifiedColumns[] = OhConfigurationPeer::OH_OPTION;
        }

    } // setOhOption()

    /**
     * Set the value of [oh_link] column.
     * 
     * @param      string $v new value
     * @return     void
     */
    public function setOhLink($v)
    {

        // Since the native PHP type for this column is string,
        // we will cast the input to a string (if it is not).
        if ($v !== null && !is_string($v)) {
            $v = (string) $v;
        }

        if ($this->oh_link !== $v || $v === '') {
            $this->oh_link = $v;
            $this->modifiedColumns[] = OhConfigurationPeer::OH_LINK;
        }

    } // setOhLink()

    /**
     * Set the value of [oh_create_date] column.
     * 
     * @param      int $v new value
     * @return     void
     */
    public function setOhCreateDate($v)
    {

        if ($v !== null && !is_int($v)) {
            $ts = strtotime($v);
            if ($ts === -1 || $ts === false) {
                throw new PropelException("Unable to parse date/time value for [oh_create_date] from input: " .
                    var_export($v, true));
            }
        } else {
            $ts = $v;
        }
        if ($this->oh_create_date !== $ts) {
            $this->oh_create_date = $ts;
            $this->modifiedColumns[] = OhConfigurationPeer::OH_CREATE_DATE;
        }

    } // setOhCreateDate()

    /**
     * Set the value of [oh_update_date] column.
     * 
     * @param      int $v new value
     * @return     void
     */
    public function setOhUpdateDate($v)
    {

        if ($v !== null && !is_int($v)) {
            $ts = strtotime($v);
            if ($ts === -1 || $ts === false) {
                throw new PropelException("Unable to parse date/time value for [oh_update_date] from input: " .
                    var_export($v, true));
            }
        } else {
            $ts = $v;
        }
        if ($this->oh_update_date !== $ts) {
            $this->oh_update_date = $ts;
            $this->modifiedColumns[] = OhConfigurationPeer::OH_UPDATE_DATE;
        }

    } // setOhUpdateDate()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (1-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param      ResultSet $rs The ResultSet class with cursor advanced to desired record pos.
     * @param      int $startcol 1-based offset column which indicates which restultset column to start with.
     * @return     int next starting column
     * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate(ResultSet $rs, $startcol = 1)
    {
        try {

            $this->oh_uid = $rs->getString($startcol + 0);

            $this->oh_title = $rs->getString($startcol + 1);

            $this->oh_folder = $rs->getString($startcol + 2);

            $this->oh_file = $rs->getString($startcol + 3);

            $this->oh_option = $rs->getString($startcol + 4);

            $this->oh_link = $rs->getString($startcol + 5);

            $this->oh_create_date = $rs->getTimestamp($startcol + 6, null);

            $this->oh_update_date = $rs->getTimestamp($startcol + 7, null);

            $this->resetModified();

            $this->setNew(false);

            // FIXME - using NUM_COLUMNS may be clearer.
            return $startcol + 8; // 8 = OhConfigurationPeer::NUM_COLUMNS - OhConfigurationPeer::NUM_LAZY_LOAD_COLUMNS).

        } catch (Exception $e) {
            throw new PropelException("Error populating OhConfiguration object", $e);
        }
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      Connection $con
     * @return     void
     * @throws     PropelException
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete($con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(OhConfigurationPeer::DATABASE_NAME);
        }

        try {
            $con->begin();
            OhConfigurationPeer::doDelete($this, $con);
            $this->setDeleted(true);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollback();
            throw $e;
        }
    }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * wraps the doSave() worker method in a transaction.
     *
     * @param      Connection $con
     * @return     int The number of rows affected by this insert/update
     * @throws     PropelException
     * @see        doSave()
     */
    public function save($con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(OhConfigurationPeer::DATABASE_NAME);
        }

        try {
            $con->begin();
            $affectedRows = $this->doSave($con);
            $con->commit();
            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollback();
            throw $e;
        }
    }

    /**
     * Stores the object in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      Connection $con
     * @return     int The number of rows affected by this insert/update and any referring
     * @throws     PropelException
     * @see        save()
     */
    protected function doSave($con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;


            // If this object has been modified, then save it to the database.
            if ($this->isModified()) {
                if ($this->isNew()) {
                    $pk = OhConfigurationPeer::doInsert($this, $con);
                    $affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
                                         // should always be true here (even though technically
                                         // BasePeer::doInsert() can insert multiple rows).

                    $this->setNew(false);
                } else {
                    $affectedRows += OhConfigurationPeer::doUpdate($this, $con);
                }
                $this->resetModified(); // [HL] After being saved an object is no longer 'modified'
            }

            $this->alreadyInSave = false;
        }
        return $affectedRows;
    } // doSave()

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return     array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param      mixed $columns Column name or an array of column names.
     * @return     boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();
            return true;
        } else {
            $this->validationFailures = $res;
            return false;
        }
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param      array $columns Array of column names to validate.
     * @return     mixed <code>true</code> if all validations pass; 
                   array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = OhConfigurationPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TYPE_PHPNAME,
     *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
     * @return     mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = OhConfigurationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        return $this->getByPosition($pos);
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return     mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch($pos) {
            case 0:
                return $this->getOhUid();
                break;
            case 1:
                return $this->getOhTitle();
                break;
            case 2:
                return $this->getOhFolder();
                break;
            case 3:
                return $this->getOhFile();
                break;
            case 4:
                return $this->getOhOption();
                break;
            case 5:
                return $this->getOhLink();
                break;
            case 6:
                return $this->getOhCreateDate();
                break;
            case 7:
                return $this->getOhUpdateDate();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param      string $keyType One of the class type constants TYPE_PHPNAME,
     *                        TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
     * @return     an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = OhConfigurationPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getOhUid(),
            $keys[1] => $this->getOhTitle(),
            $keys[2] => $this->getOhFolder(),
            $keys[3] => $this->getOhFile(),
            $keys[4] => $this->getOhOption(),
            $keys[5] => $this->getOhLink(),
            $keys[6] => $this->getOhCreateDate(),
            $keys[7] => $this->getOhUpdateDate(),
        );
        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param      string $name peer name
     * @param      mixed $value field value
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TYPE_PHPNAME,
     *                     TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM
     * @return     void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = OhConfigurationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @param      mixed $value field value
     * @return     void
     */
    public function setByPosition($pos, $value)
    {
        switch($pos) {
            case 0:
                $this->setOhUid($value);
                break;
            case 1:
                $this->setOhTitle($value);
                break;
            case 2:
                $this->setOhFolder($value);
                break;
            case 3:
                $this->setOhFile($value);
                break;
            case 4:
                $this->setOhOption($value);
                break;
            case 5:
                $this->setOhLink($value);
                break;
            case 6:
                $this->setOhCreateDate($value);
                break;
            case 7:
                $this->setOhUpdateDate($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME,
     * TYPE_NUM. The default key type is the column's phpname (e.g. 'authorId')
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return     void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = OhConfigurationPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setOhUid($arr[$keys[0]]);
        }

        if (array_key_exists($keys[1], $arr)) {
            $this->setOhTitle($arr[$keys[1]]);
        }

        if (array_key_exists($keys[2], $arr)) {
            $this->setOhFolder($arr[$keys[2]]);
        }

        if (array_key_exists($keys[3], $arr)) {
            $this->setOhFile($arr[$keys[3]]);
        }

        if (array_key_exists($keys[4], $arr)) {
            $this->setOhOption($arr[$keys[4]]);
        }

        if (array_key_exists($keys[5], $arr)) {
            $this->setOhLink($arr[$keys[5]]);
        }

        if (array_key_exists($keys[6], $arr)) {
            $this->setOhCreateDate($arr[$keys[6]]);
        }

        if (array_key_exists($keys[7], $arr)) {
            $this->setOhUpdateDate($arr[$keys[7]]);
        }

    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return     Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(OhConfigurationPeer::DATABASE_NAME);

        if ($this->isColumnModified(OhConfigurationPeer::OH_UID)) {
            $criteria->add(OhConfigurationPeer::OH_UID, $this->oh_uid);
        }

        if ($this->isColumnModified(OhConfigurationPeer::OH_TITLE)) {
            $criteria->add(OhConfigurationPeer::OH_TITLE, $this->oh_title);
        }

        if ($this->isColumnModified(OhConfigurationPeer::OH_FOLDER)) {
            $criteria->add(OhConfigurationPeer::OH_FOLDER, $this->oh_folder);
        }

        if ($this->isColumnModified(OhConfigurationPeer::OH_FILE)) {
            $criteria->add(OhConfigurationPeer::OH_FILE, $this->oh_file);
        }

        if ($this->isColumnModified(OhConfigurationPeer::OH_OPTION)) {
            $criteria->add(OhConfigurationPeer::OH_OPTION, $this->oh_option);
        }

        if ($this->isColumnModified(OhConfigurationPeer::OH_LINK)) {
            $criteria->add(OhConfigurationPeer::OH_LINK, $this->oh_link);
        }

        if ($this->isColumnModified(OhConfigurationPeer::OH_CREATE_DATE)) {
            $criteria->add(OhConfigurationPeer::OH_CREATE_DATE, $this->oh_create_date);
        }

        if ($this->isColumnModified(OhConfigurationPeer::OH_UPDATE_DATE)) {
            $criteria->add(OhConfigurationPeer::OH_UPDATE_DATE, $this->oh_update_date);
        }


        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return     Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(OhConfigurationPeer::DATABASE_NAME);

        $criteria->add(OhConfigurationPeer::OH_UID, $this->oh_uid);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return     string
     */
    public function getPrimaryKey()
    {
        return $this->getOhUid();
    }

    /**
     * Generic method to set the primary key (oh_uid column).
     *
     * @param      string $key Primary key.
     * @return     void
     */
    public function setPrimaryKey($key)
    {
        $this->setOhUid($key);
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of OhConfiguration (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @throws     PropelException
     */
    public function copyInto($copyObj, $deepCopy = false)
    {

        $copyObj->setOhTitle($this->oh_title);

        $copyObj->setOhFolder($this->oh_folder);

        $copyObj->setOhFile($this->oh_file);

        $copyObj->setOhOption($this->oh_option);

        $copyObj->setOhLink($this->oh_link);

        $copyObj->setOhCreateDate($this->oh_create_date);

        $copyObj->setOhUpdateDate($this->oh_update_date);


        $copyObj->setNew(true);

        $copyObj->setOhUid(''); // this is a pkey column, so set to default value

    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return     OhConfiguration Clone of current object.
     * @throws     PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);
        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return     OhConfigurationPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new OhConfigurationPeer();
        }
        return self::$peer;
    }
}

