<?php

require_once 'propel/map/MapBuilder.php';
include_once 'creole/CreoleTypes.php';


/**
 * This class adds structure of 'OH_CONFIGURATION' table to 'workflow' DatabaseMap object.
 *
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    workflow.classes.model.map
 */
class OhConfigurationMapBuilder
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'classes.model.map.OhConfigurationMapBuilder';

    /**
     * The database map.
     */
    private $dbMap;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
     */
    public function isBuilt()
    {
        return ($this->dbMap !== null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return     the databasemap
     */
    public function getDatabaseMap()
    {
        return $this->dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @return     void
     * @throws     PropelException
     */
    public function doBuild()
    {
        $this->dbMap = Propel::getDatabaseMap('workflow');

        $tMap = $this->dbMap->addTable('OH_CONFIGURATION');
        $tMap->setPhpName('OhConfiguration');

        $tMap->setUseIdGenerator(false);

        $tMap->addPrimaryKey('OH_UID', 'OhUid', 'string', CreoleTypes::VARCHAR, true, 32);

        $tMap->addColumn('OH_TITLE', 'OhTitle', 'string', CreoleTypes::VARCHAR, true, 200);

        $tMap->addColumn('OH_FOLDER', 'OhFolder', 'string', CreoleTypes::VARCHAR, true, 50);

        $tMap->addColumn('OH_FILE', 'OhFile', 'string', CreoleTypes::VARCHAR, false, 50);

        $tMap->addColumn('OH_OPTION', 'OhOption', 'string', CreoleTypes::VARCHAR, false, 100);

        $tMap->addColumn('OH_LINK', 'OhLink', 'string', CreoleTypes::VARCHAR, true, 200);

        $tMap->addColumn('OH_CREATE_DATE', 'OhCreateDate', 'int', CreoleTypes::TIMESTAMP, true, null);

        $tMap->addColumn('OH_UPDATE_DATE', 'OhUpdateDate', 'int', CreoleTypes::TIMESTAMP, false, null);

    } // doBuild()
}

// OhConfigurationMapBuilder

