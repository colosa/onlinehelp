
var onlineHelpGrid;
var store;
var winOnlineHelp ;

Ext.onReady(function() {
    Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
    Ext.QuickTips.init();
    
    //tool bar
    var newButton = new Ext.Action({
        text: _('ID_NEW'),
        iconCls: 'button_menu_ext ss_sprite  ss_add',
        handler: NewOnlineHelp
    });
    var editButton = new Ext.Action({
        text: _('ID_EDIT'),
        iconCls: 'button_menu_ext ss_sprite  ss_pencil',
        handler: editOnlineHelp,
        disabled: true
    });
    var deleteButton = new Ext.Action({
        text: _('ID_DELETE'),
        iconCls: 'button_menu_ext ss_sprite  ss_delete',
        handler: removeOnlineHelp,
        disabled: true
    });
    var valorCookie = getCookie('onlineHelpView');

    var importURL = new Ext.Action({
        text: 'Add PM Link',
        iconCls: 'button_menu_ext ss_sprite  ss_application_link',
        handler: importDataURL
    });
    var debugView = new Ext.Action({
        id: 'debugView',
        text: valorCookie != 'edit' ? 'View Mode' : 'Edit Mode',
        iconCls: valorCookie != 'edit' ? 'button_menu_ext ss_sprite ss_page_white' : 'button_menu_ext ss_sprite ss_page_save',
        handler: enableDisableView
    });
    
    //Field Form
    var importURLtext = new Ext.form.TextField({
        fieldLabel    : 'URL',
        name          : 'OH_URL',
        emptyText     : 'Enter URL...',
        id            : 'OH_URL',
        anchor        : '95%',
        allowBlank    : false,
        listeners     : {
            focus : function(tb, e) {
                Ext.QuickTips.register({
                    target: importURLtext,
                    title: 'Description',
                    text: "window which was opened when you press 'Help'"
                });
            },
            blur: function() {
              this.setValue(this.getValue().trim());
            }
        }
    });
    var folder = new Ext.form.TextField({
        fieldLabel    : _('ID_FOLDERS'),
        name          : 'OH_FOLDER',
        emptyText     : 'Enter a Folder...',
        id            : 'OH_FOLDER',
        anchor        : '95%',
        hidden        : true,
        readOnly      : true,
        allowBlank    : false,
        listeners     : {
            focus : function(tb, e) {
                Ext.QuickTips.register({
                    target: folder,
                    title: 'Description',
                    text: 'Parameter taken from the URL <br> http://.../sys/en/help/<b>folder</b>/file?options'
                });
            },
            blur: function() {
              this.setValue(this.getValue().trim());
            }
        }
    });
    var file = new Ext.form.TextField({
        fieldLabel    : _('ID_FILE'),
        name          : 'OH_FILE',
        id            : 'OH_FILE',
        anchor        : '95%',
        hidden        : true,
        readOnly      : true,
        allowBlank    : true,
        listeners     : {
            focus : function(tb, e) {
                Ext.QuickTips.register({
                    target: file,
                    title: 'Description',
                    text: 'Parameter taken from the URL <br> http://.../sys/en/help/folder/<b>file</b>?options'
                });
            },
            blur: function() {
              this.setValue(this.getValue().trim());
            }
        }
    });
    var option = new Ext.form.TextField({
        fieldLabel    : _('ID_OPTION'),
        name          : 'OH_OPTION',
        id            : 'OH_OPTION',
        anchor        : '95%',
        allowBlank    : true,
        listeners     : {
            focus : function(tb, e) {
                Ext.QuickTips.register({
                    target: option,
                    title: 'Description',
                    text: 'Parameter taken from the URL <br> http://.../sys/en/help/folder/file?<b>options</b>'
                });
            },
            blur: function() {
              this.setValue(this.getValue().trim());
            }
        }
    });
    var optionsProcessmaker = {
        xtype: 'fieldset',
        defaultType: 'textfield',
        title: 'Processmaker',
        items:
        [
            folder,
            file,
            option
        ]
    };
    var titleUrl = new Ext.form.TextField({
        fieldLabel    : _('ID_TITLE'),
        name          : 'OH_TITLE',
        emptyText     : 'Enter a Title...',
        id            : 'OF_TITLE',
        anchor        : '95%',
        allowBlank    : false,
        listeners     : {
            focus : function(tb, e) {
                Ext.QuickTips.register({
                    target: titleUrl,
                    title: 'Description',
                    text: 'The title of direction URL'
                });
            },
            blur: function() {
              this.setValue(this.getValue().trim());
            }
        }
    });
    var link = new Ext.form.TextField({
        fieldLabel    : _('ID_TARGET'),
        name          : 'OH_LINK',
        emptyText     : 'Enter target...',
        id            : 'OH_LINK',
        anchor        : '95%',
        allowBlank    : false,
        listeners     : {
            blur: function() {
              this.setValue(this.getValue().trim());
            },
            focus:  function(tb, e) {
                Ext.QuickTips.register({
                    target: link,
                    title: 'Description',
                    text: "window which was opened when you press 'Help'"
                });
            },
        }
    });

    var stepsFields = Ext.data.Record.create([
        {
            name : 'OH_UID',
            type: 'string'
        },
        {
            name : 'OH_TITLE',
            type: 'string'
        },
        {
            name : 'OH_FOLDER',
            type: 'string'
        },
        {
            name : 'OH_FILE',
            type: 'string'
        },
        {
            name : 'OH_OPTION',
            type: 'string'
        },
        {
            name : 'OH_LINK',
            type: 'string'
        },
        {
            name : 'OH_CREATE_DATE',
            type: 'string'
        },
        {
            name : 'OH_UPDATE_DATE',
            type: 'string'
        }
  ]);

    //Online Help List
    store = new Ext.data.GroupingStore( {
        remoteSort  : true,
        sortInfo    : stepsFields,
        proxy : new Ext.data.HttpProxy({
            url: '../onlineHelp/onlineHelpAjax',
            method: 'POST'
        }),
        reader : new Ext.data.JsonReader( {
            totalProperty: 'totalCount',
            root: 'data',
            fields : [
                {name : 'OH_UID'},
                {name : 'OH_TITLE'},
                {name : 'OH_FOLDER'},
                {name : 'OH_FILE'},
                {name : 'OH_OPTION'},
                {name : 'OH_LINK'},
                {name : 'OH_CREATE_DATE'},
                {name : 'OH_UPDATE_DATE'}
            ]
        })
    });
    store.setBaseParam( 'action', 'listOnlineHelp' );

    onlineHelpGrid = new Ext.grid.GridPanel( {
        region            : 'center',
        layout            : 'fit',
        id                : 'onlineHelpGrid',
        title             : '',
        stateful          : true,
        stateId           : 'grid',
        enableColumnResize: true,
        enableHdMenu      : true,
        frame             :false,
        columnLines       : true,
        cm: new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [
                {id:      'OH_UID',                                   dataIndex: 'OH_UID',                                hidden:true, hideable:false},
                {header:  _('ID_TITLE'),               width:  100,   dataIndex: 'OH_TITLE',          sortable: true    , hidden:false },
                {header:  _('ID_FOLDERS'),             width:  60,    dataIndex: 'OH_FOLDER',         sortable: true    , hidden:true, hideable:false},
                {header:  _('ID_FILE'),                width:  60,    dataIndex: 'OH_FILE',           sortable: true    , hidden:true, hideable:false},
                {header:  _('ID_OPTION'),              width:  80,    dataIndex: 'OH_OPTION',         sortable: true  },
                {header:  _('ID_FIELD_DYNAFORM_LINK'), width:  300,   dataIndex: 'OH_LINK',           sortable: true  },
                {header:  _('ID_CREATE_DATE'),         width:  80,    dataIndex: 'OH_CREATE_DATE',    sortable: true  },
                {header:  _('ID_LAN_UPDATE_DATE'),     width:  80,    dataIndex: 'OH_UPDATE_DATE',    sortable: true  }
            ]
        }),
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners:{
                rowselect: function(sm){
                   editButton.enable();
                   deleteButton.enable();
                },
                rowdeselect: function(sm){
                   editButton.disable();
                   deleteButton.disable();
                }
            }
        }),
        store: store,
        tbar: [importURL, '-', editButton,  '-', deleteButton, '->' , '-', debugView],
        // paging bar on the bottom
        bbar: new Ext.PagingToolbar({
            pageSize    : 25,
            store       : store,
            displayInfo : true,
            displayMsg  : 'Displaying {0} - {1} of {2}',
            autoScroll   : true
        }),
        viewConfig: {
            forceFit: true
        },
        listeners: {
            render: function(){
                this.loadMask = new Ext.LoadMask(this.body, {msg:'Loading...'});
            }
        }
    });
    store.load({params:{ start : 0 , limit : 25 }});
  
    onlineHelpGrid.addListener('rowcontextmenu', onMessageContextMenu,this);
    onlineHelpGrid.on('rowcontextmenu', function (grid, rowIndex, evt) {
      var sm = grid.getSelectionModel();
      sm.selectRow(rowIndex, sm.isSelected(rowIndex));
      var rowSelected = Ext.getCmp('onlineHelpGrid').getSelectionModel().getSelected();
    }, this);
    onlineHelpGrid.on('contextmenu', function (evt) {
      evt.preventDefault();
    }, this);

    var general = {
        xtype: 'fieldset',
        defaultType: 'textfield',
        title: 'General',
        items:
        [
            {
                xtype   : 'hidden',
                name    : 'OH_UID',
                id      : 'OH_UID'
            },
            {
                xtype     : 'displayfield',
                id        : 'OH_URL_PM',
                fieldLabel: 'URL'
            },
            optionsProcessmaker,
            titleUrl,
            link
        ]
    };

    formURL = new Ext.FormPanel({
        height      : 60,
        title       : '',
        labelWidth  : 85,
        defaults    : {xtype:'textfield'},
        bodyStyle   : 'padding: 10px',
        items       : [
            importURLtext
        ]
    });
    formGeneral = new Ext.FormPanel({
        height      : debugMode == 'edit' ? 400 : 230,
        title       : '',
        labelWidth  : 85,
        defaults    : {xtype:'textfield'},
        bodyStyle   : 'padding: 10px',
        items       : [
            general
        ]
    });

    winOnlineHelp = new Ext.Window({
        id: 'winOnlineHelp',
        title: '',
        width: 800,
        floatable: true,
        autoHeight:true,
        modal: true,
        resizable: false,
        closeAction : 'hide',
        items: [formGeneral],
        plain: true,
        buttons: [{
            id: 'btnSave',
            text: _('ID_SAVE'),
            handler : function() {
                    if(formGeneral.getForm().isValid()) {
                        formGeneral.getForm().submit({
                            url : '../onlineHelp/onlineHelpAjax',
                            params : {
                                action:'saveOnlineHelp'
                            },
                            waitMsg : 'Saving data, please wait...',
                            failure: function (form, action) {
                                Ext.MessageBox.show({
                                    title: 'Error',
                                    msg: 'Error saving the data. Please try again later.',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                                formGeneral.getForm().reset();
                            },
                            success: function (form, request) {
                                Ext.MessageBox.show({
                                    title: 'Success',
                                    msg: 'Data saved correctly.',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.INFO
                                });
                                formGeneral.getForm().reset();
                                store.load({params:{ start : 0 , limit : 25 }});
                                if (debugMode == 'edit') {
                                    window.close();
                                }
                            }
                        });
                        winOnlineHelp.hide();
                    } else {
                        Ext.MessageBox.show({
                            title: 'Error',
                            msg: 'There are required fields without filling.',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                }
            },
            {
                id: 'btnCancel',
                text: _('ID_CANCEL'),
                handler: function() { 
                    formGeneral.getForm().reset();
                    winOnlineHelp.hide();
                    if (debugMode == 'edit') {
                        window.close();
                    }
                }
            } ,
            {
                id: 'btnView',
                text: _('ID_VIEW'),
                handler: function() {
                    var linkValue = link.getValue();
                    if (linkValue.trim() != '') {
                        var w = 840;
                        var h = 500;
                        var title = 'View';
                        var left = (screen.width/2)-(w/2);
                        var top = (screen.height/2);//-(h/2);
                        var targetWinView = window.open (link.getValue(), title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
                    } else {
                        Ext.MessageBox.show({
                            title: 'Error',
                            msg: 'Enter a valid url...',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                }
            }
        ]
    });
    winImport = new Ext.Window({
        id: 'winImport',
        title: '',
        width: 800,
        floatable: true,
        autoHeight:true,
        modal: true,
        resizable: false,
        closeAction : 'hide',
        items: [ formURL ],
        plain: true,
        buttons: [{
            text: _('ID_IMPORT'),
            handler : function() {
                if(formURL.getForm().isValid()) {
                    Ext.Ajax.request({
                        url: "../onlineHelp/onlineHelpAjax",
                        method: "POST",
                        params: {
                          action:'popUp',
                          debug_mode: 'importURL',
                          Data: importURLtext.getValue()
                        },
                        success: function(response, opts) {
                          var resp = '';
                          resp = Ext.util.JSON.decode(response.responseText);
                            formGeneral.getForm().setValues({
                                OH_UID      : resp.data.debugUid,
                                OH_URL_PM   : resp.data.debugURL,
                                OH_FOLDER   : resp.data.debugFolder,
                                OH_FILE     : resp.data.debugFile,
                                OH_OPTION   : resp.data.debugOption,
                                OH_LINK     : resp.data.debugTarget
                            });
                            folder.readOnly = true;
                            file.readOnly = true;
                            if (resp.data.debugUid != '') {
                                winOnlineHelp.setTitle('Edit Register Online Help');
                            } else {
                                winOnlineHelp.setTitle('New Register Online Help');
                            }
                            winOnlineHelp.show();
                            winOnlineHelp.center();
                        },
                        failure: function(){
                            Ext.MessageBox.show({
                                title: 'Error',
                                msg: 'Error saving the data. Please try again later.',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                    });
                    winImport.hide();
                } else {
                    Ext.MessageBox.show({
                        title: 'Error',
                        msg: 'There are required fields without filling.',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
            }
          },
          {
                text: _('ID_CANCEL'),
                handler: function() { 
                    formURL.getForm().reset();
                    winImport.hide();
                    if (debugMode == 'edit') {
                        window.close();
                    }
                }
          }
        ]
    });

    function onMessageContextMenu(grid, rowIndex, e) {
        e.stopEvent();
        var coords = e.getXY();
        messageContextMenu.showAt([coords[0], coords[1]]);
    }

    if (debugMode == 'edit') {
        formGeneral.getForm().setValues({
            OH_UID      : debugUid,
            OH_URL_PM   : debugURL,
            OH_FOLDER   : debugFolder,
            OH_FILE     : debugFile,
            OH_OPTION   : debugOption,
            OH_LINK     : debugTarget
        });
        if (debugUid != '') {
            winOnlineHelp.setTitle('Edit Register Online Help (Mode Edit)');
        } else {
            winOnlineHelp.setTitle('New Register Online Help (Mode Edit)');
        }
        winOnlineHelp.show();
        winOnlineHelp.center();
    } else {
        var viewport = new Ext.Viewport({
            layout: 'border',
            autoScroll: true,
            style: {padding: ''},
            items: [
                onlineHelpGrid
            ]
        }); 
    }

});


function NewOnlineHelp(){
    formGeneral.getForm().setValues({
        OH_UID      : '',
        OH_URL_PM   : '',
        OH_FOLDER   : null,
        OH_FILE     : '',
        OH_OPTION   : '',
        OH_LINK     : null
    });
    winOnlineHelp.setTitle('New Register Online Help');
    winOnlineHelp.show();
    winOnlineHelp.center();
}
function importDataURL() {
    formURL.getForm().setValues({
        OH_URL      : ''
    });
    winImport.setTitle('Import Url');
    winImport.show();
    winImport.center();
}

function editOnlineHelp() {
    formGeneral.getForm().reset();
    var rows = onlineHelpGrid.getSelectionModel().getSelections();
    if (rows.length > 0) {
        formGeneral.getForm().setValues({
            OH_UID:rows[0].get('OH_UID'),
            OH_FOLDER:rows[0].get('OH_FOLDER'),
            OH_FILE:rows[0].get('OH_FILE'),
            OH_OPTION:rows[0].get('OH_OPTION'),
            OH_LINK:rows[0].get('OH_LINK')
        });
        winOnlineHelp.setTitle('Edit Online Help');
        winOnlineHelp.show();
        winOnlineHelp.center();
    } else {
      
    }
}

function removeOnlineHelp(){
    var rows = onlineHelpGrid.getSelectionModel().getSelections();
    if (rows.length > 0) {
        var title = rows[0].get('OH_FOLDER') + ( (rows[0].get('OH_FILE') != '') ? " -" + rows[0].get('OH_FILE') : '') + ( (rows[0].get('OH_OPTION') != '') ? " -" + rows[0].get('OH_OPTION') : '');
        winDelete = new Ext.Window({
            id        : 'winDelete',
            title     : 'Confirm',
            width     : 300,
            floatable : true,
            resizable : false,
            modal     : true,
            closeAction : 'hide',
            icon      : '/images/ext/default/window/icon-question.gif',
            html      :'<table><tr><td width="30%" align="center"><img src="/images/ext/default/window/icon-question.gif" /></td><td><span class="ext-mb-text">Do you want to delete the online Help "'+ title +'"?</span></td></tr></table>',
            plain     : true,
            buttons: [
            {
                id: 'btnwinDelete',
                text: _('ID_DELETE'),
                handler : function() {
                    Ext.Ajax.request({
                        url     : '../onlineHelp/onlineHelpAjax',
                        params  : {
                            action:'deleteOnlineHelp' ,
                            OH_UID:rows[0].get('OH_UID')
                        },
                        waitMsg : 'deleting data...',
                        failure : function (form, action) {
                            Ext.MessageBox.show({
                                title   : 'Error',
                                msg     : 'Error could not completely remove the data please try later..',
                                buttons : Ext.MessageBox.OK,
                                icon    : Ext.MessageBox.ERROR
                            });
                        },
                        success: function (form, request) {
                            Ext.MessageBox.show({
                                title   : 'Success',
                                msg     : 'Deleted data.',
                                buttons : Ext.MessageBox.OK,
                                icon    : Ext.MessageBox.INFO
                            });
                            winDelete.hide();
                            store.reload({params:{ start : 0 , limit : 25 }});
                        }
                    });
                }
            },
            {
                id      : 'btnwinCancel',
                text    : _('ID_CANCEL'),
                handler : function() {
                    winDelete.hide();
                }
            }]
        });
        winDelete.show();
        winDelete.center();
    } else {
      
    }
}
function setCookie(nombre, valor, tiempo){
    var fecha = new Date();
    fecha.setTime(fecha.getTime() + tiempo);
    fecha.setTime(fecha.getTime()+(tiempo*24*60*60*1000));
    document.cookie = nombre + ' = ' + escape(valor) + ((tiempo == null) ? '' : '; expires = ' + fecha.toGMTString()) + '; path=/';
}

function getCookie(nombre){
    var nombreCookie, cookie = null, cookies = document.cookie.split(';');
    for (i=0; i<cookies.length; i++){
      valorCookie = cookies[i].substr(cookies[i].indexOf('=') + 1);
      nombreCookie = cookies[i].substr(0,cookies[i].indexOf('=')).replace(/^\s+|\s+$/g, '');
      if (nombreCookie == nombre)
        cookie = unescape(valorCookie);
    }
    return cookie;
}
function enableDisableView () {
    valorCookie = getCookie('onlineHelpView');
    var script = debugView.lastChild.innerHTML;
    if (valorCookie == 'edit') {
        setCookie ('onlineHelpView', 'view', 1);
        var nscript = script.replace(/Edit Mode/gi,'View Mode');
        nscript = nscript.replace(/ss_page_save/gi,'ss_page_white');
    } else {
        setCookie ('onlineHelpView', 'edit', 1);
        var nscript = script.replace(/View Mode/gi,'Edit Mode');
        nscript = nscript.replace(/ss_page_white/gi,'ss_page_save');
    }
    debugView.lastChild.innerHTML = nscript
}

