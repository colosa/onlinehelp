<?php
G::LoadClass("plugin");

class onlineHelpPlugin extends enterprisePlugin
{


    public function onlineHelpPlugin($namespace, $filename = null)
    {
        $result              = parent::PMPlugin($namespace, $filename);
        $config              = parse_ini_file(PATH_PLUGINS . 'onlineHelp' . PATH_SEP . 'pluginConfig.ini');
        $this->sFriendlyName = $config['name'];
        $this->sDescription  = $config['description'];
        $this->sPluginFolder = $config['pluginFolder'];
        $this->sSetupPage    = $config['setupPage'];
        $this->iVersion      = self::getPluginVersion($namespace);
        $this->aWorkspaces   = null;
        $this->aDependences  = array(array('sClassName' => 'enterprise'), array('sClassName' => 'pmLicenseManager'));
        $this->bPrivate      = parent::registerEE($this->sPluginFolder, $this->iVersion);

        return $result;
    }

    public function setup()
    {
        $this->registerMenu('setup', 'menuSetup.php');
        if (method_exists($this, "registerCss")) {
            $this->registerCss('/plugin/onlineHelp/onlineHelpCss');
        }
    }

    public function install()
    {
        $this->checkTables();
        $this->copyInstallFiles();
    }

    public function enable()
    {
        $this->checkTables();
        $this->copyInstallFiles();
        G::rm_dir( PATH_C . 'ExtJs' );
        G::rm_dir( PATH_C . 'xmlform' );
        G::rm_dir( PATH_C . 'smarty' );
        //$uri = SYS_URI;
        //$uri = str_replace( SYS_SKIN, "help", $uri);
        //header('location: ' . $uri . "login/login");
    }

    public function disable()
    {
        //
    }
    /**
    * copy the files in data folder to the specific folders in ProcessMaker
    * @return void
    */
    public function copyInstallFiles()
    {
        if (!file_exists(PATH_CUSTOM_SKINS . "help")) {
            $this->copy_skin_folder( PATH_PLUGINS . 'onlineHelp/data/help/',
                             PATH_CUSTOM_SKINS . 'help',
                             array ("baseCss")
                );
        }
    }
    public function copy_skin_folder ($path, $dest, $exclude = array())
    {
        $defaultExcluded = array (".",".."
        );
        $excludedItems = array_merge( $defaultExcluded, $exclude );
        if (is_dir( $path )) {
            @mkdir( $dest );
            $objects = scandir( $path );
            if (sizeof( $objects ) > 0) {
                foreach ($objects as $file) {
                    if (in_array( $file, $excludedItems )) {
                        continue;
                    }
                    // go on
                    if (is_dir( $path . PATH_SEP . $file )) {
                        $this->copy_skin_folder( $path . PATH_SEP . $file, $dest . PATH_SEP . $file, $exclude );
                    } else {
                        copy( $path . PATH_SEP . $file, $dest . PATH_SEP . $file );
                    }
                }
            }
            return true;
        } elseif (is_file( $path )) {
            return copy( $path, $dest );
        } else {
            return false;
        }
    }
    /**
    * bd
    * @return void
    */
    public function checkTables()
    {
        $con = Propel::getConnection('workflow');
        $stmt = $con->createStatement();
        // setting the path of the sql schema files
        $filenameSql = PATH_PLUGINS . 'onlineHelp/data/schema.sql';
        // checking the existence of the schema file
        if (!file_exists($filenameSql)) {
            throw new Exception("File data/schema.sql doesn't exists");
        }
        // exploding the sql query in an array
        $sql = explode(';', file_get_contents($filenameSql));
        $stmt->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');
        // executing each query stored in the array
        foreach ($sql as $sentence) {
            if (trim($sentence) != '') {
                $stmt->executeQuery($sentence);
            }
        }
    }
    private static function getPluginVersion($namespace)
    {
        return "PLUGIN_VERSION";
    }
}

$oPluginRegistry = &PMPluginRegistry::getSingleton();
$oPluginRegistry->registerPlugin("onlineHelp", __FILE__);

