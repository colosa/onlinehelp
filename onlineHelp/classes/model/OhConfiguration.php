<?php

require_once 'classes/model/om/BaseOhConfiguration.php';


/**
 * Skeleton subclass for representing a row from the 'OH_CONFIGURATION' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    classes.model
 */
class OhConfiguration extends BaseOhConfiguration
{

    private $filterThisFields = array('OH_UID', 'OH_TITLE', 'OH_FOLDER', 'OH_FILE', 'OH_OPTION', 'OH_LINK', 'OH_CREATE_DATE', 'OH_UPDATE_DATE');

    public function load($abeUid)
    {
        try {
            $ohConfigurationInstance = OhConfigurationPeer::retrieveByPK($abeUid);
            $fields = $ohConfigurationInstance->toArray(BasePeer::TYPE_FIELDNAME);
            return $fields;
        } catch (Exception $error) {
            throw $error;
        }
    }

    public function createOrUpdate($data)
    {
        foreach ($data as $field => $value) {
            if (!in_array($field, $this->filterThisFields)) {
                unset($data[$field]);
            }
        }
        $connection = Propel::getConnection(OhConfigurationPeer::DATABASE_NAME);
        try {
            if (!isset($data['OH_UID'])) {
                $data['OH_UID'] = '';
            }
            if ($data['OH_UID'] == '') {
                $data['OH_UID'] = G::generateUniqueID();
                $data['OH_CREATE_DATE'] = date('Y-m-d H:i:s');
                $ohConfigurationInstance = new OhConfiguration();
            } else {
                $ohConfigurationInstance = OhConfigurationPeer::retrieveByPK($data['OH_UID']);
            }
            $data['OH_UPDATE_DATE'] = date('Y-m-d H:i:s');
            $ohConfigurationInstance->fromArray($data, BasePeer::TYPE_FIELDNAME);
            if ($ohConfigurationInstance->validate()) {
                $connection->begin();
                $result = $ohConfigurationInstance->save();
                $connection->commit();
                return $data['OH_UID'];
            } else {
                $message = '';
                $validationFailures = $ohConfigurationInstance->getValidationFailures();
                foreach ($validationFailures as $validationFailure) {
                    $message .= $validationFailure->getMessage() . '. ';
                }
                throw(new Exception('Error trying to update: ' . $message));
            }
        } catch (Exception $error) {
            $connection->rollback();
            throw $error;
        }
    }

    public function deleteByUid($oHuid)
    {
        try {
            $criteria = new Criteria('workflow');
            $criteria->add(OhConfigurationPeer::OH_UID, $oHuid);
            OhConfigurationPeer::doDelete($criteria);
        } catch (Exception $error) {
            throw $error;
        }
    }
}

// OhConfiguration

