#-----------------------------------------------------------------------------
#-- OH_CONFIGURATION
#-----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `OH_CONFIGURATION`
(
  `OH_UID`              VARCHAR(32) default '' NOT NULL,
  `OH_TITLE`            VARCHAR(50) default '' NOT NULL,
  `OH_FOLDER`           VARCHAR(50) default '' NOT NULL,
  `OH_FILE`             VARCHAR(50) default '',
  `OH_OPTION`           VARCHAR(100) default '',
  `OH_LINK`             VARCHAR(200) default '' NOT NULL,
  `OH_CREATE_DATE`     DATETIME  NOT NULL,
  `OH_UPDATE_DATE`     DATETIME,
  PRIMARY KEY (`OH_UID`)
)Engine=MyISAM DEFAULT CHARSET='utf8' COMMENT='The application folder in online Help';

INSERT INTO `OH_CONFIGURATION` (`OH_UID`, `OH_TITLE`, `OH_FOLDER`, `OH_FILE`, `OH_OPTION`, `OH_LINK`, `OH_CREATE_DATE`, `OH_UPDATE_DATE`) VALUES
('47542859551cdd910f1e038032890511', 'Logo', 'admin', 'pmLogo', '', 'http://wiki.processmaker.com/index.php/Logo', '2013-06-28 14:42:24', '2013-08-07 17:07:52'),
('93877574651cdeb7e4443f8012514194', 'Inbox', 'cases', 'casesListExtJs', 'action=todo', 'http://wiki.processmaker.com/index.php/Cases#Inbox', '2013-06-28 16:01:02', '2013-06-28 16:01:02'),
('70408300051cdf2d8db2967006411064', 'Draft', 'cases', 'casesListExtJs', 'action=draft', 'http://wiki.processmaker.com/index.php/Managing_Cases#Draft', '2013-06-28 16:32:24', '2013-06-28 16:32:24'),
('91749216051cdf31661d550015944187', 'Participated', 'cases', 'casesListExtJs', 'action=sent', 'http://wiki.processmaker.com/index.php/Managing_Cases#Participated', '2013-06-28 16:33:26', '2013-06-28 16:33:26'),
('92627510951cdf37e4bad08042111197', 'New Case', 'cases', 'casesStartPage', 'action=startCase', 'http://wiki.processmaker.com/index.php/Managing_Cases#New_Case', '2013-06-28 16:35:10', '2013-06-28 16:35:10'),
('20787225851cdf54c0543b3078529416', 'Email Settings', 'admin', 'emails', '', 'http://wiki.processmaker.com/index.php/Email_-_Settings', '2013-06-28 16:42:52', '2013-06-28 16:42:52'),
('21955897851cdf5bd5de086055290054', 'Translation', 'setup', 'languages', '', 'http://wiki.processmaker.com/index.php/Translations', '2013-06-28 16:44:45', '2013-08-08 10:41:58'),
('52665990651cdf66866cfe7030328290', 'List Skin', 'setup', 'skinsList', '', 'http://wiki.processmaker.com/index.php/ProcessMaker_Skins', '2013-06-28 16:47:36', '2013-08-07 15:09:31'),
('3027826355203b161c209c1086038005', 'Users List', 'users', 'users_List', '', 'http://wiki.processmaker.com/index.php/Managing_Users#Users', '2013-08-08 10:55:29', '2013-08-08 10:55:29'),
('7046846995203b031d29b81073337365', 'Dashboard', 'dashboard', 'renderDashletInstance', '', 'http://wiki.processmaker.com/index.php/Advanced_Dashboard', '2013-08-08 10:50:25', '2013-08-08 10:50:25')
ON DUPLICATE KEY UPDATE OH_LINK=OH_LINK;

